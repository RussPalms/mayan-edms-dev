# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Dezső József <dejo60@gmail.com>, 2024
# Roberto Rosario, 2024
# molnars <szabolcs.molnar@gmail.com>, 2024
# Csaba Tarjányi, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:29+0000\n"
"PO-Revision-Date: 2024-05-07 07:30+0000\n"
"Last-Translator: Csaba Tarjányi, 2024\n"
"Language-Team: Hungarian (https://app.transifex.com/rosarior/teams/13584/hu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:57 apps.py:99 events.py:6 links.py:154 links.py:161 menus.py:6
#: models.py:65 queues.py:9 settings.py:14 views/source_views.py:279
msgid "Sources"
msgstr "Források"

#: apps.py:109
msgid "Create a document source"
msgstr "Egy dokumentum forrás létrehozása"

#: apps.py:111
msgid ""
"Document sources are the way in which new documents are feed to Mayan EDMS, "
"create at least a web form source to be able to upload documents from a "
"browser."
msgstr ""
"A dokumentumforrások az a mód, ahogyan az új dokumentumokat a Mayan EDMS-be "
"táplálják, hozzon létre legalább egy webes űrlapforrást, hogy a "
"dokumentumokat böngészőből lehessen feltölteni."

#: apps.py:135
msgid ""
"Return the value of a specific source metadata for the document's latest "
"file."
msgstr ""

#: apps.py:137 apps.py:144
msgid "Source metadata value of"
msgstr ""

#: apps.py:143
msgid "Return the value of a specific source metadata."
msgstr ""

#: apps.py:167
msgid "Type"
msgstr "Típus"

#: events.py:10
msgid "Source created"
msgstr ""

#: events.py:13
msgid "Source edited"
msgstr ""

#: forms.py:25
msgid "An optional comment to explain the upload."
msgstr ""

#: forms.py:26
msgid "Comment"
msgstr "Hozzászólás"

#: forms.py:32
msgid "Action"
msgstr "Művelet"

#: forms.py:33
msgid ""
"The action to take in regards to the pages of the new file being uploaded."
msgstr ""

#: forms.py:54
msgid "The backend used to create the new source."
msgstr ""

#: forms.py:55
msgid "Backend"
msgstr "Háttérrendszer"

#: forms.py:71
msgid "File"
msgstr "Fájl"

#: links.py:118
msgid "New document"
msgstr ""

#: links.py:124
msgid "Upload new file"
msgstr ""

#: links.py:132
msgid "Source metadata"
msgstr ""

#: links.py:141
msgid "Create source"
msgstr ""

#: links.py:146
msgid "Delete"
msgstr "Törlés"

#: links.py:150
msgid "Edit"
msgstr "Szerkesztés"

#: links.py:167
msgid "Test"
msgstr "Tesztelés"

#: models.py:21 serializers.py:19
msgid "Document file"
msgstr "Dokumentumfájl"

#: models.py:25
msgid "Name of the source metadata entry."
msgstr ""

#: models.py:26
msgid "Key"
msgstr "Kulcs"

#: models.py:30
msgid "The actual value stored in the source metadata for the document file."
msgstr "A dokumentumfájl forrás metaadataiban tárolt tényleges érték."

#: models.py:32
msgid "Value"
msgstr "Érték"

#: models.py:40 models.py:41
msgid "Document file source metadata"
msgstr ""

#: models.py:53
msgid "A short text to describe this source."
msgstr ""

#: models.py:54
msgid "Label"
msgstr "Cimke"

#: models.py:57
msgid "Enabled"
msgstr "Engedélyezett"

#: models.py:64
msgid "Source"
msgstr "Forrás"

#: permissions.py:6
msgid "Sources setup"
msgstr "Források beállítása"

#: permissions.py:12
msgid "View document file source metadata"
msgstr "Dokumentumfájl forrás metaadatainak megtekintése"

#: permissions.py:19
msgid "Create new document sources"
msgstr "Új dokumentumforrások létrehozása"

#: permissions.py:23
msgid "Delete document sources"
msgstr "Dokumentumforrások törlése"

#: permissions.py:26
msgid "Edit document sources"
msgstr "Dokumentum források szerkesztése"

#: permissions.py:29
msgid "View existing document sources"
msgstr "Létező dokumentum források megtekintése"

#: queues.py:12
msgid "Sources periodic"
msgstr "Időszakos források"

#: queues.py:18
msgid "Handle source backend action background task"
msgstr "Forrás-háttérművelet háttérrendszer kezelése"

#: queues.py:22
msgid "Check interval source"
msgstr "Időszakos forrás ellenőrzése"

#: search.py:11 search.py:23
msgid "Source metadata key"
msgstr ""

#: search.py:16 search.py:26
msgid "Source metadata value"
msgstr ""

#: serializers.py:22 serializers.py:107
msgid "URL"
msgstr "URL"

#: serializers.py:46
msgid "Default"
msgstr "Alapéretelmezett"

#: serializers.py:49
msgid "Has default?"
msgstr "Van alapértelmezése?"

#: serializers.py:52
msgid "Help text"
msgstr ""

#: serializers.py:55 serializers.py:74
msgid "Name"
msgstr "Név"

#: serializers.py:58
msgid "Required"
msgstr "Kötelező"

#: serializers.py:70 serializers.py:85
msgid "Arguments"
msgstr "Argumentumok"

#: serializers.py:80
msgid "Accept files"
msgstr ""

#: serializers.py:84
msgid "Optional arguments for the action. Must be JSON formatted."
msgstr "Választható argumentumok a művelethez. JSON formátumúnak kell lennie."

#: serializers.py:89
msgid "Confirmation"
msgstr ""

#: serializers.py:92
msgid "Execute URL"
msgstr "URL végrehajtás"

#: serializers.py:95
msgid "Interfaces"
msgstr ""

#: serializers.py:99
msgid "name"
msgstr "név"

#: serializers.py:102
msgid "Permission"
msgstr "Engedély"

#: serializers.py:118
msgid "Binary content for the new file."
msgstr ""

#: serializers.py:154
msgid "Actions URL"
msgstr "Műveletek URL-je"

#: settings.py:21
msgid "Arguments to use when creating source backends."
msgstr "A forrás háttérrendszer létrehozásakor használandó argumentumok."

#: settings.py:29
msgid ""
"The threshold at which the SOURCES_CACHE_STORAGE_BACKEND will start deleting"
" the oldest source cache files. Specify the size in bytes."
msgstr ""

#: settings.py:37
msgid "Path to the Storage subclass used to store cached source image files."
msgstr ""
"A gyorsítótárazott forrás képfájlok tárolására használt Storage alosztály "
"elérési útja."

#: settings.py:44
msgid "Arguments to pass to the SOURCES_SOURCE_CACHE_STORAGE_BACKEND."
msgstr "Argumentumok a következőhöz: SOURCES_SOURCE_CACHE_STORAGE_BACKEND."

#: source_backend_actions/interfaces.py:161
msgid "Forms containing the upload data generated by the upload views."
msgstr ""

#: source_backend_actions/mixins/argument_help_texts.py:4
msgid "Document to which a new file will be uploaded to."
msgstr ""

#: source_backend_actions/mixins/argument_help_texts.py:8
msgid ""
"When enabled, a document stub is created immediately and returned. The "
"document file is processed asynchronously. When disabled, the entire process"
" happens asynchronously with no returned data. Enabling immediate mode, "
"disables compressed file processing."
msgstr ""
"Ha engedélyezve van, a dokumentumcsonk azonnal létrejön és visszaküldésre "
"kerül. A dokumentumfájl feldolgozása aszinkron módon történik. Ha ki van "
"kapcsolva, az egész folyamat aszinkron történik, és nem kapunk vissza "
"adatokat. Az azonnali üzemmód engedélyezése letiltja a tömörített fájl "
"feldolgozását."

#: source_backend_actions/mixins/arguments.py:31
msgid "ID of the document to which a new file will be uploaded to."
msgstr ""

#: source_backend_actions/mixins/arguments.py:59
msgid "ID of the document type."
msgstr ""

#: source_backend_actions/mixins/arguments.py:76
msgid "User that will feature as the actor in the events."
msgstr ""

#: source_backend_actions/mixins/arguments.py:81
msgid "ID of the user that will feature as the actor in the events."
msgstr ""

#: source_backends/base.py:101
msgid "General"
msgstr "Általános"

#: source_backends/base.py:134
msgid "Null backend"
msgstr "Null háttérrendszer"

#: source_backends/mixins.py:28
msgid "Regular expression used to select which files to upload."
msgstr ""

#: source_backends/mixins.py:32
msgid "Include regular expression"
msgstr "Befoglaló reguláris kifejezés"

#: source_backends/mixins.py:40
msgid "Regular expression used to exclude which files to upload."
msgstr ""

#: source_backends/mixins.py:44
msgid "Exclude regular expression"
msgstr "Kizáró reguláris kifejezés"

#: source_backends/mixins.py:58
msgid "Content selection"
msgstr ""

#: storages.py:13
msgid ""
"Unable to initialize the staging folder file image storage. Check the "
"settings {} and {} for formatting errors."
msgstr ""
"Nem lehet inicializálni az átmeneti mappa fájl képtárát. Ellenőrizze a "
"beállításokat {} és {} formázási hibákért."

#: storages.py:20
msgid "Sources cache"
msgstr ""

#: tests/arguments.py:8
msgid "Python file like object."
msgstr "Python fájl-szerű objektum."

#: tests/arguments.py:12
msgid "ID of the shared uploaded file to be processed."
msgstr "A feldolgozandó megosztott feltöltött fájl azonosítója."

#: views/base.py:33
msgid ""
"There are no enabled sources that support this operation. Create a new one "
"or enable and existing one."
msgstr ""

#: views/document_file_views.py:41
msgid ""
"This means that the sources system did not record any information about the "
"creation of the document file."
msgstr ""

#: views/document_file_views.py:45
msgid "No source metadata available for this document file."
msgstr ""

#: views/document_file_views.py:49
#, python-format
msgid "Source metadata for: %(document_file)s"
msgstr ""

#: views/document_file_views.py:82
#, python-format
msgid "Unable to upload new files for document \"%(document)s\". %(exception)s"
msgstr ""
"Nem lehet új fájlokat feltölteni a \"%(document)s\". %(exception)s "
"dokumentumhoz"

#: views/document_file_views.py:126
#, python-format
msgid "Error executing document file upload task; %(exception)s"
msgstr ""

#: views/document_file_views.py:145
msgid "New document file queued for upload and will be available shortly."
msgstr ""

#: views/document_file_views.py:169
#, python-format
msgid "Upload a new file for document \"%(document)s\" from source: %(source)s"
msgstr ""
"Új fájl feltöltése a \"%(document)s\" dokumentumhoz a %(source)s forrásból"

#: views/document_file_views.py:175
msgid "Submit"
msgstr "Beküldés"

#: views/document_views.py:53
#, python-format
msgid "Error processing source document upload; %(exception)s"
msgstr "Hiba a forrásdokumentum feltöltés feldolgozása során; %(exception)s"

#: views/document_views.py:74
msgid "New document queued for upload and will be available shortly."
msgstr ""

#: views/document_views.py:94
#, python-format
msgid "Upload a document of type \"%(document_type)s\" from source: %(source)s"
msgstr ""
"Egy \"%(document_type)s\" típusú dokumentumo feltöltése a %(source)s "
"forrásból"

#: views/source_views.py:76
#, python-format
msgid "Unable to execute action; %s."
msgstr "Nem lehet végrehajtani a műveletet; %s ."

#: views/source_views.py:155
#, python-format
msgid "Error processing source action; %(exception)s"
msgstr ""

#: views/source_views.py:170
msgid "Source action executed successfully."
msgstr ""

#: views/source_views.py:179
msgid "New source backend selection"
msgstr "Új forrás háttérrendszer választás"

#: views/source_views.py:207
#, python-format
msgid "Create a \"%s\" source"
msgstr "\"%s\" forrás létrehozása"

#: views/source_views.py:233
#, python-format
msgid "Delete the source: %s?"
msgstr "Forrás törlése: %s ?"

#: views/source_views.py:249
#, python-format
msgid "Edit source: %s"
msgstr ""

#: views/source_views.py:273
msgid ""
"Sources provide the means to upload documents. Some sources are interactive "
"and require user input to operate. Others are automatic and run in the "
"background without user intervention."
msgstr ""

#: views/source_views.py:278
msgid "No sources available"
msgstr ""

#: views/source_views.py:303
#, python-format
msgid ""
"The selected action \"%s\" does not require confirmation and cannot be "
"tested."
msgstr ""
"A \"%s\" kiválasztott művelet nem igényel megerősítést, és nem tesztelhető."

#: views/source_views.py:323
msgid ""
"This will execute the source code even if the source is not enabled. Sources"
" that delete content after downloading will not do so while being tested. "
"Check the source's error log for information during testing. A successful "
"test will clear the error log."
msgstr ""

#: views/source_views.py:329
#, python-format
msgid "Trigger check for source \"%s\"?"
msgstr "Eseményindító ellenőrzés a \"%s\" forráshoz?"

#: views/source_views.py:345
msgid ""
"Source test queued. Check for newly created documents or for error log "
"entries."
msgstr ""

#: wizard_steps.py:13
msgid "Select document type"
msgstr "Dokumentumtípus választás"

#: wizards.py:68
msgid ""
"No interactive document sources have been defined or none have been enabled,"
" create one before proceeding."
msgstr ""
"Nincs interaktív dokumentumforrás definiálva, vagy egyik sincs engedélyezve,"
" hozzon létre egyet a folytatás előtt."

#: wizards.py:85
#, python-format
msgid "Step %(step)d of %(total_steps)d: %(step_label)s"
msgstr "%(total_steps)d lépés a %(step)d-ből: %(step_label)s"

#: wizards.py:91
msgid "Document upload wizard"
msgstr ""

#: wizards.py:100
msgid "First"
msgstr ""

#: wizards.py:106
msgid "Previous"
msgstr ""

#: wizards.py:113
msgid "Next"
msgstr "Következő"
