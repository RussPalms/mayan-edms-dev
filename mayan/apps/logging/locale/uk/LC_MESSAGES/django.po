# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Vasyl Olenchenko <olenchenko@gmail.com>, 2024
# Leo Lado, 2024
# Kostiantyn Tyshkovets <shukach_13@ukr.net>, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:29+0000\n"
"PO-Revision-Date: 2024-05-07 07:30+0000\n"
"Last-Translator: Kostiantyn Tyshkovets <shukach_13@ukr.net>, 2024\n"
"Language-Team: Ukrainian (https://app.transifex.com/rosarior/teams/13584/uk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: uk\n"
"Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);\n"

#: apps.py:24 events.py:6 permissions.py:6 settings.py:12
msgid "Logging"
msgstr "Логування"

#: apps.py:40
msgid "System"
msgstr "Система"

#: apps.py:64
msgid "Domain"
msgstr ""

#: events.py:10
msgid "Error log deleted"
msgstr "Журнал помилок видалено"

#: links.py:15 views.py:36
msgid "Global error log"
msgstr "Глобальний журнал помилок"

#: links.py:26
msgid "Delete"
msgstr "Видалити"

#: links.py:31
msgid "Errors"
msgstr "Помилки"

#: links.py:37
msgid "Clear errors"
msgstr "Очистити помилки"

#: model_mixins.py:11
#, python-format
msgid "Unknown domain name: %s"
msgstr ""

#: model_mixins.py:18
msgid "Object"
msgstr "Об'єкт"

#: model_mixins.py:26
msgid "App label"
msgstr "Мітка програми"

#: models.py:21 models.py:41
msgid "Internal name"
msgstr "Внутрішнє ім'я"

#: models.py:26 models.py:38
msgid "Error log"
msgstr "Журнал помилок"

#: models.py:27
msgid "Error logs"
msgstr "Журнали помилок"

#: models.py:55 models.py:69
msgid "Error log partition"
msgstr "Розділ журналу помилок"

#: models.py:56
msgid "Error log partitions"
msgstr "Розділи журналу помилок"

#: models.py:73
msgid "Date and time"
msgstr "Дата і час"

#: models.py:78
msgid "Domain name"
msgstr ""

#: models.py:81
msgid "Text"
msgstr "Текст"

#: models.py:89
msgid "Error log partition entry"
msgstr "Запис розділу журналу помилок"

#: models.py:90
msgid "Error log partition entries"
msgstr "Записи розділу журналу помилок"

#: permissions.py:10
msgid "Delete error log"
msgstr "Видалити журнал помилок"

#: permissions.py:13
msgid "View error log"
msgstr "Переглянути журнал помилок"

#: serializers.py:12
msgid "Content type"
msgstr "Тип вмісту"

#: serializers.py:16
msgid "Object ID"
msgstr "ID об'єкта"

#: serializers.py:19
msgid "URL"
msgstr "URL"

#: settings.py:18
msgid "Disable logging message ANSI color codes."
msgstr "Вимкнути реєстрацію повідомлень про кольорові коди ANSI."

#: settings.py:24
msgid "Automatically enable logging to all apps."
msgstr "Автоматично ввімкнути журналювання для всіх програм."

#: settings.py:30
msgid "List of handlers to which logging messages will be sent."
msgstr ""
"Список обробників, на які будуть надсилатися повідомлення журналування."

#: settings.py:35
msgid "Level for the logging system."
msgstr "Рівень для системи журналювання."

#: settings.py:41
msgid "Path to the logfile that will track errors during production."
msgstr "Шлях до файлу журналу, який відстежуватиме помилки в продакшені."

#: views.py:30
msgid ""
"This view displays the error log of different objects. An empty list is a "
"good thing."
msgstr ""
"Цей вигляд відображає журнал помилок різних об'єктів. Порожній список - це "
"хороша річ."

#: views.py:34 views.py:99
msgid "There are no error log entries"
msgstr "Немає записів у журналі помилок"

#: views.py:50
#, python-format
msgid "Clear error log entries for: %s"
msgstr "Очистити записи журналу помилок для: %s"

#: views.py:57
msgid "Object error log cleared successfully"
msgstr "Об'єкт журналу помилок успішно очищено"

#: views.py:74
#, python-format
msgid "Delete error log entry: %s"
msgstr "Видалити запис про помилку в журналі: %s"

#: views.py:95
msgid ""
"This view displays the error log of an object. An empty list is a good "
"thing."
msgstr ""
"Цей вигляд відображає журнал помилок об'єкта. Порожній список - це хороша "
"річ."

#: views.py:103
#, python-format
msgid "Error log entries for: %s"
msgstr "Записи журналу помилок для: %s"
